package com.FirstInFirstOut.Entity;
import java.io.Serializable;

import javax.persistence.GenerationType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;




@Entity
@Table(name="sortieproduit")
public class SortieProduit implements Serializable {
	
	
	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	

	private int id_sortie;
	private int num_sortie;
	private LocalDateTime  date_sortie;


	
	
//	@OneToMany
//	@JoinColumn(name="id_produit",referencedColumnName="id_produit",insertable=false, updatable=false)
//	private AchatProduit achatproduit;
	
	
//    @ManyToOne @JoinColumn(name="id_fournisseur", nullable=false,insertable=false, updatable=false)
//    private Fournisseur fournisseur;
	
	
	
	
	public SortieProduit() {
		super();
		// TODO Auto-generated constructor stub
	}
  
	
	



	public SortieProduit( int id_sortie, int num_sortie, LocalDateTime date_sortie) {
		super();
		this.id_sortie = id_sortie;
		this.num_sortie = num_sortie;
		this.date_sortie = date_sortie;
		
	}






	public int getId__sortie() {
		return id_sortie;
	}




	public void setId__sortie(int id__sortie) {
		this.id_sortie = id__sortie;
	}




	public int getNum_sortie() {
		return num_sortie;
	}




	public void setNum_sortie(int num_sortie) {
		this.num_sortie = num_sortie;
	}




	public LocalDateTime getDate_sortie() {
		return date_sortie;
	}




	public void setDate_sortie(LocalDateTime date_sortie) {
		this.date_sortie = date_sortie;
	}




	


	

	
}
