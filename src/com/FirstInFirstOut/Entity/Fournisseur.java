package com.FirstInFirstOut.Entity;
import java.io.Serializable;


import javax.persistence.GenerationType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;




@Entity
@Table(name="fournisseur")
public class Fournisseur implements Serializable {
	
	
	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_fournisseur;
	private String labelle;
	
	
//	@OneToMany
//	@JoinColumn(name="id_produit",referencedColumnName="id_produit",insertable=false, updatable=false)
//	private Produit produit;
	
//    @OneToOne
//    @JoinColumn(name="id_fournisseur",referencedColumnName="id_fournisseur",insertable=false, updatable=false)
//     private EntreeDetaill entreedetaill;
//    
//    @OneToOne
//    @JoinColumn(name="id_fournisseur",referencedColumnName="id_fournisseur",insertable=false, updatable=false)
//     private SortieDetaill sortiedetaill;
//    
//    @OneToOne
//    @JoinColumn(name="id_fournisseur",referencedColumnName="id_fournisseur",insertable=false, updatable=false)
//     private DetaillDemande DetaillDemande;
//	 

	
	
	
	public Fournisseur() {
		super();
		// TODO Auto-generated constructor stub
	}

	


	public Fournisseur(int id_fournisseur, String labelle) {
		super();
		this.id_fournisseur = id_fournisseur;
		this.labelle = labelle;
	}




	public int getId_Fournisseur() {
		return id_fournisseur;
	}



	public void setId_Fournisseur(int id_fournisseur) {
		this.id_fournisseur = id_fournisseur;
	}



	public String getLabelle() {
		return labelle;
	}



	public void setLabelle(String labelle) {
		this.labelle = labelle;
	}





	


	
}
