package com.FirstInFirstOut.Entity;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.GenerationType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table(name="entreedetaill")
public class EntreeDetaill implements Serializable {
	
	
	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	private int id_entree_detaill;
	private int id_produit_entree;
	private int id_produit;	
	private int id_fournisseur;
	private int quantite_achat;
	private float prix_achat;
	private int rest_quantite;
	private int num_reception;
	private Date  date_reception;

	
	
   

//	@OneToMany( targetEntity=Produit.class, mappedBy="id_produit" )
//	private List<Produit> produits = new ArrayList<>();
//
//	@OneToMany( targetEntity=Fournisseur.class, mappedBy="id_fournisseur" )
//	private List<Fournisseur> fournisseurs = new ArrayList<>();
//	
	
	//
//@OneToMany( targetEntity=Produit.class, mappedBy="id_produit" )
//	private List<Produit> produits = new ArrayList<>();


	
//@OneToMany
//@JoinColumn(name="id_produit",referencedColumnName="id_produit",insertable=false, updatable=false)
//private Produit produit;


	
	
  
	public EntreeDetaill() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	


	public EntreeDetaill(int id_entree_detaill, int id_produit_entree, int id_produit, int id_fournisseur,
			int quantite_achat, float prix_achat, int rest_quantite, int num_reception,
			Date date_reception) {
		super();
		this.id_entree_detaill = id_entree_detaill;
		this.id_produit_entree = id_produit_entree;
		this.id_produit = id_produit;
		this.id_fournisseur = id_fournisseur;
		this.quantite_achat = quantite_achat;
		this.prix_achat = prix_achat;
		this.rest_quantite = rest_quantite;
		this.num_reception = num_reception;
		this.date_reception = date_reception;
		
	}





	public int getId_entree_detaill() {
		return id_entree_detaill;
	}





	public void setId_entree_detaill(int id_entree_detaill) {
		this.id_entree_detaill = id_entree_detaill;
	}





	public int getId_produit_entree() {
		return id_produit_entree;
	}





	public void setId_produit_entree(int id_produit_entree) {
		this.id_produit_entree = id_produit_entree;
	}





	public int getId_produit() {
		return id_produit;
	}





	public void setId_produit(int id_produit) {
		this.id_produit = id_produit;
	}





	public int getId_fournisseur() {
		return id_fournisseur;
	}





	public void setId_fournisseur(int id_fournisseur) {
		this.id_fournisseur = id_fournisseur;
	}





	public int getQuantite_achat() {
		return quantite_achat;
	}





	public void setQuantite_achat(int quantite_achat) {
		this.quantite_achat = quantite_achat;
	}





	public float getPrix_achat() {
		return prix_achat;
	}





	public void setPrix_achat(float prix_achat) {
		this.prix_achat = prix_achat;
	}





	public int getRest_quantite() {
		return rest_quantite;
	}





	public void setRest_quantite(int rest_quantite) {
		this.rest_quantite = rest_quantite;
	}





	public int getNum_reception() {
		return num_reception;
	}





	public void setNum_reception(int num_reception) {
		this.num_reception = num_reception;
	}





	public Date getDate_reception() {
		return date_reception;
	}





	public void setDate_reception(Date date_reception) {
		this.date_reception = date_reception;
	}





	





	
}
