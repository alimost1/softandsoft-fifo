package com.FirstInFirstOut.Entity;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.GenerationType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table(name="demande")
public class Demande implements Serializable {
	
	
	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_demande;
	private Date date_demande;

	
	
	
//	 @OneToMany
//	    @JoinTable( name = "sortiedetaill",
//	                joinColumns = @JoinColumn( name = "id_produit" ),
//	                inverseJoinColumns = @JoinColumn( name = "id_detaill_sortie" ) )
//	    private List<SortieDetaill> sortiedetaill = new ArrayList<>();
//	 
//	 
//	 @OneToMany
//	    @JoinTable( name = "achatproduit",
//	                joinColumns = @JoinColumn( name = "id_produit" ),
//	                inverseJoinColumns = @JoinColumn( name = "id_achat" ) )
//	    private List<EntreeProduit> achatproduit = new ArrayList<>();

	
//	@OneToMany
//	@JoinColumn(name="id_fournisseur",referencedColumnName="id_fournisseur",insertable=false, updatable=false)
//	private Fournisseur fournisseur;
//	
	
	public Demande() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	




	public Demande(int id_demande, Date date_demande) {
	super();
	this.id_demande = id_demande;
	this.date_demande = date_demande;
}






	public int getId_demande() {
		return id_demande;
	}




	public void setId_demande(int id_demande) {
		this.id_demande = id_demande;
	}




	public Date getDate_demande() {
		return date_demande;
	}




	public void setDate_demande(Date date_demande) {
		this.date_demande = date_demande;
	}
	
	
	


	
	
	

	
}
