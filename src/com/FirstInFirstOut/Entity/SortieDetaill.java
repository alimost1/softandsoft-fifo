package com.FirstInFirstOut.Entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.GenerationType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;





@Entity
@Table(name="sortiedetaill")
public class SortieDetaill implements Serializable {
	
	
	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_sortie_detaill;
	private int id_fournisseur;
	private int id_sortie;
	private int id_produit;
	private int num_reception;
	private float prix_sortie;
	private float quantite_sortie;



	
	
//	@OneToMany
//	@JoinColumn(name="id_produit",referencedColumnName="id_produit",insertable=false, updatable=false)
//	private AchatProduit achatproduit;
		
//@OneToMany( targetEntity=Produit.class, mappedBy="id_produit" )
//private List<Produit> produits = new ArrayList<>();
//
//@OneToMany( targetEntity=Fournisseur.class, mappedBy="id_fournisseur" )
//private List<Fournisseur> fournisseurs = new ArrayList<>();
//	
	public SortieDetaill() {
		super();
		// TODO Auto-generated constructor stub
	}





public SortieDetaill(int id_sortie_detaill, int id_fournisseur, int id_sortie, int id_produit, int num_reception,
		float prix_sortie, float quantite_sortie) {
	super();
	this.id_sortie_detaill = id_sortie_detaill;
	this.id_fournisseur = id_fournisseur;
	this.id_sortie = id_sortie;
	this.id_produit = id_produit;
	this.num_reception = num_reception;
	this.prix_sortie = prix_sortie;
	this.quantite_sortie = quantite_sortie;
}





public int getId_sortie_detaill() {
	return id_sortie_detaill;
}





public void setId_sortie_detaill(int id_sortie_detaill) {
	this.id_sortie_detaill = id_sortie_detaill;
}





public int getId_fournisseur() {
	return id_fournisseur;
}





public void setId_fournisseur(int id_fournisseur) {
	this.id_fournisseur = id_fournisseur;
}





public int getId_sortie() {
	return id_sortie;
}





public void setId_sortie(int id_sortie) {
	this.id_sortie = id_sortie;
}





public int getId_produit() {
	return id_produit;
}





public void setId_produit(int id_produit) {
	this.id_produit = id_produit;
}





public int getNum_reception() {
	return num_reception;
}






public void setNum_reception(int num_reception) {
	this.num_reception = num_reception;
}





public float getPrix_sortie() {
	return prix_sortie;
}





public void setPrix_sortie(float prix_sortie) {
	this.prix_sortie = prix_sortie;
}





public float getQuantite_sortie() {
	return quantite_sortie;
}





public void setQuantite_sortie(float quantite_sortie) {
	this.quantite_sortie = quantite_sortie;
}
	
	
	

	
	
	
	
}
