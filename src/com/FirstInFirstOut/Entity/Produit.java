package com.FirstInFirstOut.Entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.GenerationType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;



@Entity
@Table(name="produit")
public class Produit implements Serializable {
	
	
	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_produit;
	private int num_produit;
	private String labelle;
	
	
//	@OneToOne( targetEntity=EntreeDetaill.class, mappedBy="id_produit" )
//	private List<EntreeDetaill> entreeDetaill = new ArrayList<>();
//	
//	@OneToOne( targetEntity=SortieDetaill.class, mappedBy="id_produit" )
//	private List<SortieDetaill> sortieDetaill = new ArrayList<>();
	

	
	
	
	
	@OneToOne
    @JoinColumn(name="id_produit",referencedColumnName="id_produit",insertable=false, updatable=false)
     private EntreeDetaill EntreeDetaill;
    
    @OneToOne
    @JoinColumn(name="id_produit",referencedColumnName="id_produit",insertable=false, updatable=false)
     private SortieDetaill sortiedetaill;
   
    @OneToOne
    @JoinColumn(name="id_produit",referencedColumnName="id_produit",insertable=false, updatable=false)
     private DetaillDemande DetaillDemande;
//	 
	
//	@OneToMany
//	    @JoinTable( name = "sortiedetaill",
//	                joinColumns = @JoinColumn( name = "id_produit" ),
//	                inverseJoinColumns = @JoinColumn( name = "id_detaill_sortie" ) )
//	    private List<SortieDetaill> sortiedetaill = new ArrayList<>();
//	 
	 


	
//	@OneToMany
//	@JoinColumn(name="id_fournisseur",referencedColumnName="id_fournisseur",insertable=false, updatable=false)
//	private Fournisseur fournisseur;
//	
	
	public Produit() {
		super();
		// TODO Auto-generated constructor stub
	}



	public Produit(int id_produit, int num_produit, String labelle) {
		super();
		this.id_produit = id_produit;
		this.num_produit = num_produit;
		this.labelle = labelle;
	}






	public int getId_produit() {
		return id_produit;
	}






	public void setId_produit(int id_produit) {
		this.id_produit = id_produit;
	}






	public int getNum_produit() {
		return num_produit;
	}






	public void setNum_produit(int num_produit) {
		this.num_produit = num_produit;
	}






	public String getLabelle() {
		return labelle;
	}






	public void setLabelle(String labelle) {
		this.labelle = labelle;
	}





	
}
