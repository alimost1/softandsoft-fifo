package com.FirstInFirstOut.Entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;




@Entity
@Table(name="detailldemande")
public class DetaillDemande implements Serializable {
	
	
	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_detaill_demande;
	private int num_demande;
	private int id_produit;
	private float quantite_demamde;
	
	
	
	
//	@OneToMany
//	@JoinColumn(name="id_produit",referencedColumnName="id_produit",insertable=false, updatable=false)
//	private Produit produit;
	

//
//	@OneToMany( targetEntity=Produit.class, mappedBy="id_produit" )
//	private List<Produit> produits = new ArrayList<>();


	
	
	
	
	public DetaillDemande(int id_detaill_demande, int num_demande, int id_produit, float quantite_demamde,
			Produit produit) {
		super();
		this.id_detaill_demande = id_detaill_demande;
		this.num_demande = num_demande;
		this.id_produit = id_produit;
		this.quantite_demamde = quantite_demamde;
//		this.produit = produit;
	}



	public DetaillDemande() {
		super();
		// TODO Auto-generated constructor stub
	}



	public int getId_detaill_demande() {
		return id_detaill_demande;
	}



	public void setId_detaill_demande(int id_detaill_demande) {
		this.id_detaill_demande = id_detaill_demande;
	}



	public int getNum_demande() {
		return num_demande;
	}



	public void setNum_demande(int num_demande) {
		this.num_demande = num_demande;
	}



	public int getId_produit() {
		return id_produit;
	}



	public void setId_produit(int id_produit) {
		this.id_produit = id_produit;
	}



	public float getQuantite_demamde() {
		return quantite_demamde;
	}



	public void setQuantite_demamde(float quantite_demamde) {
		this.quantite_demamde = quantite_demamde;
	}



//	public Produit getProduit() {
//		return produit;
//	}
//
//
//
//	public void setProduit(Produit produit) {
//		this.produit = produit;
//	}
	
	
	



	
}
