package com.FirstInFirstOut.Entity;
import java.io.Serializable;
import javax.persistence.GenerationType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Date;


@Entity
@Table(name="entreeproduit")
public class EntreeProduit implements Serializable {
	
	
	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_produit_entree;
	private Date  date_achat;

	
	
	
//	@OneToOne
//	@JoinColumn(name="id_produit",referencedColumnName="id_produit",insertable=false, updatable=false)
//	private Produit produit;
//	
//   @OneToMany
//   @JoinColumn(name="id_fournisseur",referencedColumnName="id_founnisseur",insertable=false, updatable=false)
//    private Fournisseur fournisseur;
   
//   @ManyToOne @JoinColumn(name="id_produit", nullable=false, insertable=false, updatable=false)
//   private Produit produit;
	
	
	
	public EntreeProduit() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	



	public EntreeProduit(int id_produit_entree, Date date_achat, Produit produit) {
		super();
		this.id_produit_entree = id_produit_entree;
		this.date_achat = date_achat;
		
	}






	public int getId_produit_entree() {
		return id_produit_entree;
	}



	public void setId_produit_entree(int id_produit_entree) {
		this.id_produit_entree = id_produit_entree;
	}



	public Date getDate_achat() {
		return date_achat;
	}



	public void setDate_achat(Date date_achat) {
		this.date_achat = date_achat;
	}



	
	
	
	
	

	
	
}
