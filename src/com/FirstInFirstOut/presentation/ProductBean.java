package com.FirstInFirstOut.presentation;

import com.FirstInFirstOut.Entity.Produit;
import com.FirstInFirstOut.Entity.Fournisseur;


//import com.soft.ProductManager.dao.ProductDAO;
//import com.soft.ProductManager.dao.ProductDAOImp;

import com.FirstInFirstOut.services.ProduitServices;
import com.FirstInFirstOut.services.ProduitServicesImp;

import com.FirstInFirstOut.services.FournisseurServices;
import com.FirstInFirstOut.services.FournisseurServicesImp;

import com.FirstInFirstOut.services.SortieServices;
import com.FirstInFirstOut.services.SortieServicesImp;
import com.FirstInFirstOut.services.EntreeProduitServices;
import com.FirstInFirstOut.services.EntreeProduitServicesImp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;


@ManagedBean(name = "productBean3")
@RequestScoped

public class ProductBean {

	private ProduitServices produitServices = new ProduitServicesImp();
	private FournisseurServices fournisseurServices = new FournisseurServicesImp();
	private SortieServices sortieServices = new SortieServicesImp();
	private EntreeProduitServices achatProduitServices = new EntreeProduitServicesImp();

	private String name;
	private String labelleproduit;
	private String produit;
	private String numproduit;
	private String labellefournisseur;
	private String fournisseurlist;
	private String id_produit;
	private String id_Fournisseur;
	private String operationeditproduit;
	private String operationeditfournisseur;
	private Boolean showForm=true;
	private List<SelectItem> productlist;
	private List<Produit> productlist1;
	private List<SelectItem> Fournisseurlist;
	public String[] favNumber4;


/**
 * 
 */
@PostConstruct
	public void initBean() {
	
	/**
	 * fournisseur list 
	 */
	showForm = true;
	Fournisseurlist = new ArrayList<>();
	Fournisseurlist.add(new SelectItem("",""));
	
	List<Fournisseur> listServices1 = fournisseurServices.findAll();
	
	for(Fournisseur o2:listServices1){
		
		Fournisseurlist.add(new SelectItem(o2.getId_Fournisseur(),o2.getLabelle()));
		
	}
		
		
	/*******************************************************************************
	 * Fournisseurlist 
	 ********************************************************************************/
		
		
	productlist = new ArrayList<>();
	productlist.add(new SelectItem("",""));
		
		List<Produit> listServices11 = produitServices.findAll();
		
		for(Produit o2:listServices11){
			
			productlist.add(new SelectItem(o2.getId_produit(),o2.getLabelle()));
			
		}
		
		productlist1 = produitServices.findAll();
		
		
		
/*******************************************************************************
		 * edit product 
********************************************************************************/
		if ("edit".equalsIgnoreCase(getParam("operationeditproduit"))) {
			setOperationeditproduit(getParam("operationeditproduit"));
			Integer id_produit = null;
			Produit produit = null;
			try {
				id_produit = Integer.valueOf(getParam("id_produit"));
				setId_produit(getParam("id_produit"));

			} catch (Exception e) {

				if (id_produit != null) {
					produit = produitServices.findById(id_produit);

					if (produit != null) {
						labelleproduit = produit.getLabelle();
//						
//						if (produit.getNum_produit() != null) {
//
//						numproduit = produit.getNum_produit();
//						}
//						
						

					}

				}

			}
		}


}

						


	
/*******************************************************************************
 * Add product 
********************************************************************************/



	public void ddproduct(ActionEvent e) {
		
	
		Produit p = null;
		if("edit".equalsIgnoreCase(operationeditproduit)) {
			p = produitServices.findById(new Integer(id_produit));
			
		}
		else {
			p= new Produit ();
			
		}
		
		p.setNum_produit(Integer.valueOf(numproduit));
		p.setLabelle(labelleproduit);
		
		
	
       
		
		produitServices.add(p);
		
		productlist1 = produitServices.findAll();
		
		
		labelleproduit = "";
		numproduit = "";
		id_produit = "";

		
	}
	
	/*******************************************************************************
	 * Add fournisseur 
	********************************************************************************/



		public void addfournisseur(ActionEvent e) {
			
		
			Fournisseur F = null;
			if("edit".equalsIgnoreCase(operationeditfournisseur)) {
				F = fournisseurServices.findById(new Integer(id_Fournisseur));
				
			}
			else {
				F= new Fournisseur ();
				
			}
			
			
			F.setLabelle(labellefournisseur);
			
			
			
			fournisseurServices.add(F);
			
			productlist1 = produitServices.findAll();
			
			
			labelleproduit = "";
			id_Fournisseur = "";


			
		
			
		}
		

	
	/*******************************************************************************
	 *  delete
	********************************************************************************/	

	public void deletProduct(ActionEvent e) {

		FacesContext fc = FacesContext.getCurrentInstance();
		Map<String, String> param = fc.getExternalContext().getRequestParameterMap();

		produitServices.delete(new Integer(param.get("id")));
		
		productlist1 = produitServices.findAll();
        


	}
	
	public String getParam(String name) {
		FacesContext fc = FacesContext.getCurrentInstance();
		Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
		return params.get(name);
		
	}
	
	

	
	public void showFormAction (ActionEvent e) {
		showForm =true; 
	}
	
	public void HidFormAction (ActionEvent e) {
		showForm =true; 
	}



	public ProductBean() {
		super();
		// TODO Auto-generated constructor stub
	}



	public ProduitServices getProduitServices() {
		return produitServices;
	}



	public void setProduitServices(ProduitServices produitServices) {
		this.produitServices = produitServices;
	}



	public FournisseurServices getFournisseurServices() {
		return fournisseurServices;
	}



	public void setFournisseurServices(FournisseurServices fournisseurServices) {
		this.fournisseurServices = fournisseurServices;
	}



	public SortieServices getSortieServices() {
		return sortieServices;
	}



	public void setSortieServices(SortieServices sortieServices) {
		this.sortieServices = sortieServices;
	}



	public EntreeProduitServices getAchatProduitServices() {
		return achatProduitServices;
	}



	public void setAchatProduitServices(EntreeProduitServices achatProduitServices) {
		this.achatProduitServices = achatProduitServices;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getLabelleproduit() {
		return labelleproduit;
	}



	public void setLabelleproduit(String labelleproduit) {
		this.labelleproduit = labelleproduit;
	}



	public String getProduit() {
		return produit;
	}



	public void setProduit(String produit) {
		this.produit = produit;
	}



	public String getNumproduit() {
		return numproduit;
	}



	public void setNumproduit(String numproduit) {
		this.numproduit = numproduit;
	}



	


	public String getLabellefournisseur() {
		return labellefournisseur;
	}



	public void setLabellefournisseur(String labellefournisseur) {
		this.labellefournisseur = labellefournisseur;
	}



	public String getFournisseurlist1() {
		return fournisseurlist;
	}



	public void setFournisseurlist(String fournisseurlist) {
		this.fournisseurlist = fournisseurlist;
	}





	public String getId_produit() {
		return id_produit;
	}



	public void setId_produit(String id_produit) {
		this.id_produit = id_produit;
	}



	public String getOperationeditproduit() {
		return operationeditproduit;
	}



	public void setOperationeditproduit(String operationeditproduit) {
		this.operationeditproduit = operationeditproduit;
	}



	public Boolean getShowForm() {
		return showForm;
	}



	public void setShowForm(Boolean showForm) {
		this.showForm = showForm;
	}



	public List<SelectItem> getProductlist() {
		return productlist;
	}



	public void setProductlist(List<SelectItem> productlist) {
		this.productlist = productlist;
	}



	public List<Produit> getProductlist1() {
		return productlist1;
	}



	public void setProductlist1(List<Produit> productlist1) {
		this.productlist1 = productlist1;
	}



	public List<SelectItem> getFournisseurlist() {
		return Fournisseurlist;
	}



	public void setFournisseurlist(List<SelectItem> fournisseurlist) {
		Fournisseurlist = fournisseurlist;
	}





	public String getOperationeditfournisseur() {
		return operationeditfournisseur;
	}





	public void setOperationeditfournisseur(String operationeditfournisseur) {
		this.operationeditfournisseur = operationeditfournisseur;
	}





	public String getId_Fournisseur() {
		return id_Fournisseur;
	}





	public void setId_Fournisseur(String id_Fournisseur) {
		this.id_Fournisseur = id_Fournisseur;
	}





	

   
}
