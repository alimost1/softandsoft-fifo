package com.FirstInFirstOut.presentation;

import com.FirstInFirstOut.Entity.Produit;
import com.FirstInFirstOut.Entity.Fournisseur;
import com.FirstInFirstOut.Entity.EntreeProduit;
import com.FirstInFirstOut.Entity.EntreeDetaill;
import com.FirstInFirstOut.Entity.SortieProduit;
import com.FirstInFirstOut.Entity.SortieDetaill;
import com.FirstInFirstOut.Entity.Demande;
import com.FirstInFirstOut.Entity.DetaillDemande;



import com.FirstInFirstOut.services.ProduitServices;
import com.FirstInFirstOut.services.ProduitServicesImp;
import com.FirstInFirstOut.services.DemandeServices;
import com.FirstInFirstOut.services.DemandeServicesImp;
import com.FirstInFirstOut.services.EntreeDetaillServices;
import com.FirstInFirstOut.services.EntreeDetaillServicesImp;
import com.FirstInFirstOut.services.FournisseurServices;
import com.FirstInFirstOut.services.FournisseurServicesImp;
import com.FirstInFirstOut.services.SortieServices;
import com.FirstInFirstOut.services.SortieServicesImp;
import com.FirstInFirstOut.services.EntreeProduitServices;
import com.FirstInFirstOut.services.EntreeProduitServicesImp;
import com.FirstInFirstOut.services.DetaillDemandeServices;
import com.FirstInFirstOut.services.DetaillDemandeServicesImp;
import com.FirstInFirstOut.services.SortieDetaillServices;
import com.FirstInFirstOut.services.SortieDetaillServicesImp;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import java.time.LocalDateTime;

@ManagedBean(name = "product")
@RequestScoped
public class Productbean1 {
	
	private ProduitServices produitServices = new ProduitServicesImp();
	private FournisseurServices fournisseurServices = new FournisseurServicesImp();
	private DemandeServices demandeServices = new DemandeServicesImp();
	private EntreeDetaillServices entreeDetaillServices = new EntreeDetaillServicesImp();
	private SortieServices sortieServices = new SortieServicesImp();
	private EntreeProduitServices entreeProduitServices = new EntreeProduitServicesImp();
	private DetaillDemandeServices detaillDemandeServices = new DetaillDemandeServicesImp();
	private SortieDetaillServices sortieDetaillServices = new SortieDetaillServicesImp();



	private String name;
	private String id_produit;
	private String num_produit;
	private String id_produit_entree;
	private String date_achat;
	private String labelle;
	private String id_entree_detaill;
	private String labellefournisseur;
	private String id_fournisseur;
	private String quantite_achat;
	private String prix_achat;
	private String rest_quantite;
	private String num_reception;
	private String id_Fournisseur;
	private String date_reception;
	private String operationaddachat;
	private String operationentree;
	private String  operationentreedetaill;
	private Boolean showForm=true;
	private List<SelectItem> productlist;
	private List<EntreeProduit> entreeproduit;
	private List<SelectItem> Fournisseurlist;
	private List<EntreeDetaill> entreedetaill;
	private List<Produit> product1;
	private String[] course4;
	private boolean existing;
	
	
	
	
	
	
	
	
	public ProduitServices getProduitServices() {
		return produitServices;
	}
	public void setProduitServices(ProduitServices produitServices) {
		this.produitServices = produitServices;
	}
	public FournisseurServices getFournisseurServices() {
		return fournisseurServices;
	}
	public void setFournisseurServices(FournisseurServices fournisseurServices) {
		this.fournisseurServices = fournisseurServices;
	}
	public DemandeServices getDemandeServices() {
		return demandeServices;
	}
	public void setDemandeServices(DemandeServices demandeServices) {
		this.demandeServices = demandeServices;
	}
	public EntreeDetaillServices getEntreeDetaillServices() {
		return entreeDetaillServices;
	}
	public void setEntreeDetaillServices(EntreeDetaillServices entreeDetaillServices) {
		this.entreeDetaillServices = entreeDetaillServices;
	}
	public SortieServices getSortieServices() {
		return sortieServices;
	}
	public void setSortieServices(SortieServices sortieServices) {
		this.sortieServices = sortieServices;
	}
	public EntreeProduitServices getEntreeProduitServices() {
		return entreeProduitServices;
	}
	public void setEntreeProduitServices(EntreeProduitServices entreeProduitServices) {
		this.entreeProduitServices = entreeProduitServices;
	}
	public DetaillDemandeServices getDetaillDemandeServices() {
		return detaillDemandeServices;
	}
	public void setDetaillDemandeServices(DetaillDemandeServices detaillDemandeServices) {
		this.detaillDemandeServices = detaillDemandeServices;
	}
	public SortieDetaillServices getSortieDetaillServices() {
		return sortieDetaillServices;
	}
	public void setSortieDetaillServices(SortieDetaillServices sortieDetaillServices) {
		this.sortieDetaillServices = sortieDetaillServices;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId_produit() {
		return id_produit;
	}
	public void setId_produit(String id_produit) {
		this.id_produit = id_produit;
	}
	public String getNum_produit() {
		return num_produit;
	}
	public void setNum_produit(String num_produit) {
		this.num_produit = num_produit;
	}
	public String getId_produit_entree() {
		return id_produit_entree;
	}
	public void setId_produit_entree(String id_produit_entree) {
		this.id_produit_entree = id_produit_entree;
	}
	public String getDate_achat() {
		return date_achat;
	}
	public void setDate_achat(String date_achat) {
		this.date_achat = date_achat;
	}
	public String getLabelle() {
		return labelle;
	}
	public void setLabelle(String labelle) {
		this.labelle = labelle;
	}
	public String getId_entree_detaill() {
		return id_entree_detaill;
	}
	public void setId_entree_detaill(String id_entree_detaill) {
		this.id_entree_detaill = id_entree_detaill;
	}
	public String getLabellefournisseur() {
		return labellefournisseur;
	}
	public void setLabellefournisseur(String labellefournisseur) {
		this.labellefournisseur = labellefournisseur;
	}
	public String getId_fournisseur() {
		return id_fournisseur;
	}
	public void setId_fournisseur(String id_fournisseur) {
		this.id_fournisseur = id_fournisseur;
	}
	public String getQuantite_achat() {
		return quantite_achat;
	}
	public void setQuantite_achat(String quantite_achat) {
		this.quantite_achat = quantite_achat;
	}
	public String getPrix_achat() {
		return prix_achat;
	}
	public void setPrix_achat(String prix_achat) {
		this.prix_achat = prix_achat;
	}
	public String getRest_quantite() {
		return rest_quantite;
	}
	public void setRest_quantite(String rest_quantite) {
		this.rest_quantite = rest_quantite;
	}
	public String getNum_reception() {
		return num_reception;
	}
	public void setNum_reception(String num_reception) {
		this.num_reception = num_reception;
	}
	public String getId_Fournisseur() {
		return id_Fournisseur;
	}
	public void setId_Fournisseur(String id_Fournisseur) {
		this.id_Fournisseur = id_Fournisseur;
	}
	public String getDate_reception() {
		return date_reception;
	}
	public void setDate_reception(String date_reception) {
		this.date_reception = date_reception;
	}
	public String getOperationaddachat() {
		return operationaddachat;
	}
	public void setOperationaddachat(String operationaddachat) {
		this.operationaddachat = operationaddachat;
	}
	public String getOperationentree() {
		return operationentree;
	}
	public void setOperationentree(String operationentree) {
		this.operationentree = operationentree;
	}
	public String getOperationentreedetaill() {
		return operationentreedetaill;
	}
	public void setOperationentreedetaill(String operationentreedetaill) {
		this.operationentreedetaill = operationentreedetaill;
	}
	public Boolean getShowForm() {
		return showForm;
	}
	public void setShowForm(Boolean showForm) {
		this.showForm = showForm;
	}
	public List<SelectItem> getProductlist() {
		return productlist;
	}
	public void setProductlist(List<SelectItem> productlist) {
		this.productlist = productlist;
	}
	public List<EntreeProduit> getEntreeproduit() {
		return entreeproduit;
	}
	public void setEntreeproduit(List<EntreeProduit> entreeproduit) {
		this.entreeproduit = entreeproduit;
	}
	public List<SelectItem> getFournisseurlist() {
		return Fournisseurlist;
	}
	public void setFournisseurlist(List<SelectItem> fournisseurlist) {
		Fournisseurlist = fournisseurlist;
	}
	public List<EntreeDetaill> getEntreedetaill() {
		return entreedetaill;
	}
	public void setEntreedetaill(List<EntreeDetaill> entreedetaill) {
		this.entreedetaill = entreedetaill;
	}
	public List<Produit> getProduct1() {
		return product1;
	}
	public void setProduct1(List<Produit> product1) {
		this.product1 = product1;
	}
	public String[] getCourse4() {
		return course4;
	}
	public void setCourse4(String[] course4) {
		this.course4 = course4;
	}
	public boolean isExisting() {
		return existing;
	}
	public void setExisting(boolean existing) {
		this.existing = existing;
	}
	


    
}