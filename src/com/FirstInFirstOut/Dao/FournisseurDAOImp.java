package com.FirstInFirstOut.Dao;

import java.util.List;

import org.hibernate.Session;



import com.FirstInFirstOut.Entity.Fournisseur;
import com.FirstInFirstOut.util.HibernateUtil;



public class FournisseurDAOImp implements FournisseurDAO {
	
	Session session = HibernateUtil.openSession();


	@Override
	public void add(Fournisseur e) {
		try {
	        session.getTransaction().begin();
			session.save(e);
			session.getTransaction().commit();
		}
		catch (RuntimeException ex) {
		    session.getTransaction().rollback();
		}
	}
	

	@Override
	public void delete(int id_fournisseur) {
		try {
			session.getTransaction().begin();
			Fournisseur p = findById(id_fournisseur);
			session.delete(p);
			session.getTransaction().commit();
		}
		catch (RuntimeException ex) {
		    session.getTransaction().rollback();
		}
	
	}
	
	
	@Override
	public Fournisseur edit(Fournisseur e) {
		session.getTransaction().begin();
		Fournisseur p = (Fournisseur) session.merge(e);
		session.getTransaction().commit();
		return p;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Fournisseur> findAll() {
		return session.createQuery("from  Fournisseur ").list();

	}

	

	@Override
	public Fournisseur findById(int id_fournisseur) {
		return (Fournisseur) session.get(Fournisseur.class, id_fournisseur);
	}
	

}