package com.FirstInFirstOut.Dao;
import java.util.List;
import org.hibernate.Session;
import com.FirstInFirstOut.Entity.EntreeProduit;
import com.FirstInFirstOut.util.HibernateUtil;


public class EntreeProduitDAOImp implements EntreeProduitDAO {
	
	Session session = HibernateUtil.openSession();


	@Override
	public void add(EntreeProduit e) {
		try {
	        session.getTransaction().begin();
			session.save(e);
			session.getTransaction().commit();
		}
		catch (RuntimeException ex) {
		    session.getTransaction().rollback();
		}
	}
	

	@Override
	public void delete(int id_produit_entree) {
		try {
			session.getTransaction().begin();
			EntreeProduit p = findById(id_produit_entree);
			session.delete(p);
			session.getTransaction().commit();
		}
		catch (RuntimeException ex) {
		    session.getTransaction().rollback();
		}
	
	}
	
	
	@Override
	public EntreeProduit edit(EntreeProduit e) {
		session.getTransaction().begin();
		EntreeProduit p = (EntreeProduit) session.merge(e);
		session.getTransaction().commit();
		return p;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<EntreeProduit> findAll() {
		return session.createQuery("from  EntreeProduit ").list();

	}

	

	@Override
	public EntreeProduit findById(int id_produit_entree) {
		return (EntreeProduit) session.get(EntreeProduit.class, id_produit_entree);
	}
	

}