package com.FirstInFirstOut.Dao;

import java.util.List;

import org.hibernate.Session;



import com.FirstInFirstOut.Entity.EntreeDetaill;
import com.FirstInFirstOut.util.HibernateUtil;



public class EntreeDetaillDAOImp implements EntreeDetaillDAO {
	
	Session session = HibernateUtil.openSession();


	@Override
	public void add(EntreeDetaill e) {
		try {
	        session.getTransaction().begin();
			session.save(e);
			session.getTransaction().commit();
		}
		catch (RuntimeException ex) {
		    session.getTransaction().rollback();
		}
	}
	

	@Override
	public void delete(int id_entree_detaill) {
		try {
			session.getTransaction().begin();
			EntreeDetaill p = findById(id_entree_detaill);
			session.delete(p);
			session.getTransaction().commit();
		}
		catch (RuntimeException ex) {
		    session.getTransaction().rollback();
		}
	
	}
	
	
	@Override
	public EntreeDetaill edit(EntreeDetaill e) {
		session.getTransaction().begin();
		EntreeDetaill p = (EntreeDetaill) session.merge(e);
		session.getTransaction().commit();
		return p;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<EntreeDetaill> findAll() {
		return session.createQuery("from  entreeDetaill ").list();

	}

	

	@Override
	public EntreeDetaill findById(int id_entree_detaill) {
		return (EntreeDetaill) session.get(EntreeDetaill.class, id_entree_detaill);
	}
	

}
