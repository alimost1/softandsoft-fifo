package com.FirstInFirstOut.Dao;

import java.util.List;

import org.hibernate.Session;



import com.FirstInFirstOut.Entity.SortieDetaill;
import com.FirstInFirstOut.util.HibernateUtil;



public class SortieDetaillDAOImp implements SortieDetaillDAO {
	
	Session session = HibernateUtil.openSession();


	@Override
	public void add(SortieDetaill e) {
		try {
	        session.getTransaction().begin();
			session.save(e);
			session.getTransaction().commit();
		}
		catch (RuntimeException ex) {
		    session.getTransaction().rollback();
		}
	}
	

	@Override
	public void delete(int id_sortie_detaill) {
		try {
			session.getTransaction().begin();
			SortieDetaill p = findById(id_sortie_detaill);
			session.delete(p);
			session.getTransaction().commit();
		}
		catch (RuntimeException ex) {
		    session.getTransaction().rollback();
		}
	
	}
	
	
	@Override
	public SortieDetaill edit(SortieDetaill e) {
		session.getTransaction().begin();
		SortieDetaill p = (SortieDetaill) session.merge(e);
		session.getTransaction().commit();
		return p;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<SortieDetaill> findAll() {
		return session.createQuery("from  SortieDetaill ").list();

	}

	

	@Override
	public SortieDetaill findById(int id_sortie_detaill) {
		return (SortieDetaill) session.get(SortieDetaill.class, id_sortie_detaill);
	}
	

}
