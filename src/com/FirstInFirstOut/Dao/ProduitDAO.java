package com.FirstInFirstOut.Dao;

import java.util.List;

import com.FirstInFirstOut.Entity.Produit;



public interface ProduitDAO {
	
	public void add(Produit e);
	
	public Produit edit(Produit e);
	
	public void  delete(int id_produit);
	
	public List<Produit> findAll();
	
	public Produit findById(int id_produit);
	
	

}