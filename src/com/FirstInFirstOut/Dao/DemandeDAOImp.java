package com.FirstInFirstOut.Dao;

import java.util.List;

import org.hibernate.Session;



import com.FirstInFirstOut.Entity.Demande;
import com.FirstInFirstOut.util.HibernateUtil;



public class DemandeDAOImp implements DemandeDAO {
	
	Session session = HibernateUtil.openSession();


	@Override
	public void add(Demande e) {
		try {
	        session.getTransaction().begin();
			session.save(e);
			session.getTransaction().commit();
		}
		catch (RuntimeException ex) {
		    session.getTransaction().rollback();
		}
	}
	

	@Override
	public void delete(int id_demande) {
		try {
			session.getTransaction().begin();
			Demande p = findById(id_demande);
			session.delete(p);
			session.getTransaction().commit();
		}
		catch (RuntimeException ex) {
		    session.getTransaction().rollback();
		}
	
	}
	
	
	@Override
	public Demande edit(Demande e) {
		session.getTransaction().begin();
		Demande p = (Demande) session.merge(e);
		session.getTransaction().commit();
		return p;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Demande> findAll() {
		return session.createQuery("from  Produit ").list();

	}

	
	

	@Override
	public Demande findById(int id_demande) {
		return (Demande) session.get(Demande.class, id_demande);
	}
	
}