package com.FirstInFirstOut.Dao;

import java.util.List;

import org.hibernate.Session;



import com.FirstInFirstOut.Entity.Produit;
import com.FirstInFirstOut.util.HibernateUtil;



public class ProduitDAOImp implements ProduitDAO {
	
	Session session = HibernateUtil.openSession();


	@Override
	public void add(Produit e) {
		try {
	        session.getTransaction().begin();
			session.save(e);
			session.getTransaction().commit();
		}
		catch (RuntimeException ex) {
		    session.getTransaction().rollback();
		}
	}
	

	@Override
	public void delete(int id_produit) {
		try {
			session.getTransaction().begin();
			Produit p = findById(id_produit);
			session.delete(p);
			session.getTransaction().commit();
		}
		catch (RuntimeException ex) {
		    session.getTransaction().rollback();
		}
	
	}
	
	
	@Override
	public Produit edit(Produit e) {
		session.getTransaction().begin();
		Produit p = (Produit) session.merge(e);
		session.getTransaction().commit();
		return p;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Produit> findAll() {
		return session.createQuery("from  Produit ").list();

	}

	
	

	@Override
	public Produit findById(int id_produit) {
		return (Produit) session.get(Produit.class, id_produit);
	}
	
}