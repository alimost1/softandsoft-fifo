package com.FirstInFirstOut.Dao;

import java.util.List;

import com.FirstInFirstOut.Entity.EntreeProduit;



public interface EntreeProduitDAO {
	
	public void add(EntreeProduit e);
	
	public EntreeProduit edit(EntreeProduit e);
	
	public void  delete(int id_produit_entree);
	
	public List<EntreeProduit> findAll();
	
	public EntreeProduit findById(int id_produit_entree);
	
	

}