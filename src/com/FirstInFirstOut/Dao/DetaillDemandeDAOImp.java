package com.FirstInFirstOut.Dao;

import java.util.List;

import org.hibernate.Session;



import com.FirstInFirstOut.Entity.DetaillDemande;
import com.FirstInFirstOut.util.HibernateUtil;



public class DetaillDemandeDAOImp implements DetaillDemandeDAO {
	
	Session session = HibernateUtil.openSession();


	@Override
	public void add(DetaillDemande e) {
		try {
	        session.getTransaction().begin();
			session.save(e);
			session.getTransaction().commit();
		}
		catch (RuntimeException ex) {
		    session.getTransaction().rollback();
		}
	}
	

	@Override
	public void delete(int id_detaill_demande) {
		try {
			session.getTransaction().begin();
			DetaillDemande p = findById(id_detaill_demande);
			session.delete(p);
			session.getTransaction().commit();
		}
		catch (RuntimeException ex) {
		    session.getTransaction().rollback();
		}
	
	}
	
	
	@Override
	public DetaillDemande edit(DetaillDemande e) {
		session.getTransaction().begin();
		DetaillDemande p = (DetaillDemande) session.merge(e);
		session.getTransaction().commit();
		return p;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<DetaillDemande> findAll() {
		return session.createQuery("from  DetaillDemande ").list();

	}

	
	

	@Override
	public DetaillDemande findById(int id_detaill_demande) {
		return (DetaillDemande) session.get(DetaillDemande.class, id_detaill_demande);
	}
	
}