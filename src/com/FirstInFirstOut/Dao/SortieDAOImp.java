package com.FirstInFirstOut.Dao;

import java.util.List;

import org.hibernate.Session;



import com.FirstInFirstOut.Entity.SortieProduit;
import com.FirstInFirstOut.util.HibernateUtil;



public class SortieDAOImp implements SortieDAO {
	
	Session session = HibernateUtil.openSession();


	@Override
	public void add(SortieProduit e) {
		try {
	        session.getTransaction().begin();
			session.save(e);
			session.getTransaction().commit();
		}
		catch (RuntimeException ex) {
		    session.getTransaction().rollback();
		}
	}
	

	@Override
	public void delete(int id_sortie) {
		try {
			session.getTransaction().begin();
			SortieProduit p = findById(id_sortie);
			session.delete(p);
			session.getTransaction().commit();
		}
		catch (RuntimeException ex) {
		    session.getTransaction().rollback();
		}
	
	}
	
	
	@Override
	public SortieProduit edit(SortieProduit e) {
		session.getTransaction().begin();
		SortieProduit p = (SortieProduit) session.merge(e);
		session.getTransaction().commit();
		return p;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<SortieProduit> findAll() {
		return session.createQuery("from  SortieProduit ").list();

	}

	

	@Override
	public SortieProduit findById(int id_sortie) {
		return (SortieProduit) session.get(SortieProduit.class, id_sortie);
	}
	

}
