package com.FirstInFirstOut.Dao;

import java.util.List;

import com.FirstInFirstOut.Entity.DetaillDemande;



public interface DetaillDemandeDAO {
	
	public void add(DetaillDemande e);
	
	public DetaillDemande edit(DetaillDemande e);
	
	public void  delete(int id_detaill_demande);
	
	public List<DetaillDemande> findAll();
	
	public DetaillDemande findById(int id_detaill_demande);
	
	

}
