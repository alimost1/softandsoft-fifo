package com.FirstInFirstOut.Dao;

import java.util.List;

import com.FirstInFirstOut.Entity.Demande;



public interface DemandeDAO {
	
	public void add(Demande e);
	
	public Demande edit(Demande e);
	
	public void  delete(int id_demande);
	
	public List<Demande> findAll();
	
	public Demande findById(int id_demande);
	
	

}