package com.FirstInFirstOut.Dao;

import java.util.List;

import com.FirstInFirstOut.Entity.SortieDetaill;



public interface SortieDetaillDAO {
	
	public void add(SortieDetaill e);
	
	public SortieDetaill edit(SortieDetaill e);
	
	public void  delete(int id_sortie_detaill);
	
	public List<SortieDetaill> findAll();
	
	public SortieDetaill findById(int id_sortie_detaill);
	
	

}