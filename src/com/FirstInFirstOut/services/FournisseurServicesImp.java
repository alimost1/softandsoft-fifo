package com.FirstInFirstOut.services;

import java.util.List;
import com.FirstInFirstOut.Dao.FournisseurDAO;
import com.FirstInFirstOut.Dao.FournisseurDAOImp;
import com.FirstInFirstOut.Entity.Fournisseur;

public class FournisseurServicesImp implements FournisseurServices  {
	
	FournisseurDAO dao = new FournisseurDAOImp();

	
	@Override
	public void add(Fournisseur e) {
		
		 dao.add(e);	
		
	}

	@Override
	public Fournisseur edit(Fournisseur e) {
		
		return dao.edit(e);
	}

	@Override
	public void delete(int id_fournisseur) {
		
		 dao.delete(id_fournisseur);
	}

	@Override
	public List<Fournisseur> findAll() {
		
		return dao.findAll();
	
	}

	@Override
	public Fournisseur findById(int id_fournisseur) {
		
		return dao.findById(id_fournisseur);
	}

	

}

