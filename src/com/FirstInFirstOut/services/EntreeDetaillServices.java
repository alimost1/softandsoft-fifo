package com.FirstInFirstOut.services;

import java.util.List;

import com.FirstInFirstOut.Entity.EntreeDetaill;



public interface EntreeDetaillServices {
	
	public void add(EntreeDetaill e);
	
	public EntreeDetaill edit(EntreeDetaill e);
	
	public void  delete(int id_entree_detaill);
	
	public List<EntreeDetaill> findAll();
	
	public EntreeDetaill findById(int id_entree_detaill);
	
	

}