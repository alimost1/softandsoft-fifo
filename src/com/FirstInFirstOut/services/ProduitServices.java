package com.FirstInFirstOut.services;
import java.util.List;
import com.FirstInFirstOut.Entity.Produit;



public interface ProduitServices {
	
	public void add(Produit e);
	
	public Produit edit(Produit e);
	
	public void  delete(int id_produit);
	
	public List<Produit> findAll();
	
	public Produit findById(int id_produit);

	
	
}