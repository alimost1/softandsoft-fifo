package com.FirstInFirstOut.services;

import java.util.List;


import com.FirstInFirstOut.Dao.SortieDetaillDAO;
import com.FirstInFirstOut.Dao.SortieDetaillDAOImp;
import com.FirstInFirstOut.Entity.SortieDetaill;


public class SortieDetaillServicesImp implements SortieDetaillServices  {
	
	SortieDetaillDAO dao = new SortieDetaillDAOImp();

	
	@Override
	public void add(SortieDetaill e) {
		
		 dao.add(e);	
		
	}

	@Override
	public SortieDetaill edit(SortieDetaill e) {
		
		return dao.edit(e);
	}

	@Override
	public void delete(int id_sortie_detaill) {
		
		 dao.delete(id_sortie_detaill);
	}

	@Override
	public List<SortieDetaill> findAll() {
		
		return dao.findAll();
	
	}

	@Override
	public SortieDetaill findById(int id_sortie_detaill) {
		
		return dao.findById(id_sortie_detaill);
	}

	

}
