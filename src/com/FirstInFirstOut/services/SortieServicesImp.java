package com.FirstInFirstOut.services;

import java.util.List;
import com.FirstInFirstOut.Dao.SortieDAO;
import com.FirstInFirstOut.Dao.SortieDAOImp;
import com.FirstInFirstOut.Entity.SortieProduit;

public class SortieServicesImp implements SortieServices  {
	
	SortieDAO dao = new SortieDAOImp();

	
	@Override
	public void add(SortieProduit e) {
		
		 dao.add(e);	
		
	}

	@Override
	public SortieProduit edit(SortieProduit e) {
		
		return dao.edit(e);
	}

	@Override
	public void delete(int id_sortie) {
		
		 dao.delete(id_sortie);
	}

	@Override
	public List<SortieProduit> findAll() {
		
		return dao.findAll();
	
	}

	@Override
	public SortieProduit findById(int id_sortie) {
		
		return dao.findById(id_sortie);
	}

	

}
