package com.FirstInFirstOut.services;

import java.util.List;


import com.FirstInFirstOut.Dao.EntreeDetaillDAO;
import com.FirstInFirstOut.Dao.EntreeDetaillDAOImp;
import com.FirstInFirstOut.Entity.EntreeDetaill;


public class EntreeDetaillServicesImp implements EntreeDetaillServices  {
	
	EntreeDetaillDAO dao = new EntreeDetaillDAOImp();

	
	@Override
	public void add(EntreeDetaill e) {
		
		 dao.add(e);	
		
	}

	@Override
	public EntreeDetaill edit(EntreeDetaill e) {
		
		return dao.edit(e);
	}

	@Override
	public void delete(int id_entree_detaill) {
		
		 dao.delete(id_entree_detaill);
	}

	@Override
	public List<EntreeDetaill> findAll() {
		
		return dao.findAll();
	
	}

	@Override
	public EntreeDetaill findById(int id_entree_detaill) {
		
		return dao.findById(id_entree_detaill);
	}

	

}
