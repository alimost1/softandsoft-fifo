package com.FirstInFirstOut.services;

import java.util.List;
import com.FirstInFirstOut.Dao.EntreeProduitDAO;
import com.FirstInFirstOut.Dao.EntreeProduitDAOImp;
import com.FirstInFirstOut.Entity.EntreeProduit;

public class EntreeProduitServicesImp implements EntreeProduitServices  {
	
	EntreeProduitDAO dao = new EntreeProduitDAOImp();

	
	@Override
	public void add(EntreeProduit e) {
		
		 dao.add(e);	
		
	}

	@Override
	public EntreeProduit edit(EntreeProduit e) {
		
		return dao.edit(e);
	}

	@Override
	public void delete(int id_produit_entree) {
		
		 dao.delete(id_produit_entree);
	}

	@Override
	public List<EntreeProduit> findAll() {
		
		return dao.findAll();
	
	}

	@Override
	public EntreeProduit findById(int id_produit_entree) {
		
		return dao.findById(id_produit_entree);
	}

	

}