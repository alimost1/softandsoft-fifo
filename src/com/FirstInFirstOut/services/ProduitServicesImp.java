package com.FirstInFirstOut.services;

import java.util.List;
import com.FirstInFirstOut.Dao.ProduitDAO;
import com.FirstInFirstOut.Dao.ProduitDAOImp;
import com.FirstInFirstOut.Entity.Produit;

public class ProduitServicesImp implements ProduitServices  {
	
	ProduitDAO dao = new ProduitDAOImp();

	
	@Override
	public void add(Produit e) {
		
		 dao.add(e);	
		
	}

	@Override
	public Produit edit(Produit e) {
		
		return dao.edit(e);
	}

	@Override
	public void delete(int id_produit) {
		
		 dao.delete(id_produit);
	}

	@Override
	public List<Produit> findAll() {
		
		return dao.findAll();
	
	}

	@Override
	public Produit findById(int id_produit) {
		
		return dao.findById(id_produit);
	}

	

}
