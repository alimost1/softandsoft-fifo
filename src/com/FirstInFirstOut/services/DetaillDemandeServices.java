package com.FirstInFirstOut.services;

import java.util.List;

import com.FirstInFirstOut.Entity.DetaillDemande;



public interface DetaillDemandeServices {
	
	public void add(DetaillDemande id_detaill_demande);
	
	public DetaillDemande edit(DetaillDemande e);
	
	public void  delete(int id_detaill_demande);
	
	public List<DetaillDemande> findAll();
	
	public DetaillDemande findById(int id_detaill_demande);
	
	

}