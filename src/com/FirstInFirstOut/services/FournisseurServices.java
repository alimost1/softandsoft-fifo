package com.FirstInFirstOut.services;
import java.util.List;
import com.FirstInFirstOut.Entity.Fournisseur;



public interface FournisseurServices {
	
	public void add(Fournisseur e);
	
	public Fournisseur edit(Fournisseur e);
	
	public void  delete(int id_fournisseur);
	
	public List<Fournisseur> findAll();
	
	public Fournisseur findById(int id_fournisseur);
	
}
