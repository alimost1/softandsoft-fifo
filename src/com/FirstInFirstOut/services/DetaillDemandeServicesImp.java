package com.FirstInFirstOut.services;

import java.util.List;
import com.FirstInFirstOut.Dao.DetaillDemandeDAO;
import com.FirstInFirstOut.Dao.DetaillDemandeDAOImp;
import com.FirstInFirstOut.Entity.DetaillDemande;

public class DetaillDemandeServicesImp implements DetaillDemandeServices  {
	
	DetaillDemandeDAO dao = new DetaillDemandeDAOImp();

	
	@Override
	public void add(DetaillDemande e) {
		
		 dao.add(e);	
		
	}

	@Override
	public DetaillDemande edit(DetaillDemande e) {
		
		return dao.edit(e);
	}

	@Override
	public void delete(int id_detaill_demande) {
		
		 dao.delete(id_detaill_demande);
	}

	@Override
	public List<DetaillDemande> findAll() {
		
		return dao.findAll();
	
	}

	@Override
	public DetaillDemande findById(int id_detaill_demande) {
		
		return dao.findById(id_detaill_demande);
	}

	

}