package com.FirstInFirstOut.services;

import java.util.List;

import com.FirstInFirstOut.Entity.Demande;



public interface DemandeServices {
	
	public void add(Demande id_demande);
	
	public Demande edit(Demande e);
	
	public void  delete(int id_demande);
	
	public List<Demande> findAll();
	
	public Demande findById(int id_demande);
	
	

}