package com.FirstInFirstOut.services;

import java.util.List;
import com.FirstInFirstOut.Dao.DemandeDAO;
import com.FirstInFirstOut.Dao.DemandeDAOImp;
import com.FirstInFirstOut.Entity.Demande;

public class DemandeServicesImp implements DemandeServices  {
	
	DemandeDAO dao = new DemandeDAOImp();

	
	@Override
	public void add(Demande e) {
		
		 dao.add(e);	
		
	}

	@Override
	public Demande edit(Demande e) {
		
		return dao.edit(e);
	}

	@Override
	public void delete(int id_demande) {
		
		 dao.delete(id_demande);
	}

	@Override
	public List<Demande> findAll() {
		
		return dao.findAll();
	
	}

	@Override
	public Demande findById(int id_demande) {
		
		return dao.findById(id_demande);
	}

	

}