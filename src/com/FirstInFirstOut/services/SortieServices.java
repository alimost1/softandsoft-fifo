package com.FirstInFirstOut.services;
import java.util.List;
import com.FirstInFirstOut.Entity.SortieProduit;



public interface SortieServices {
	
	public void add(SortieProduit e);
	
	public SortieProduit edit(SortieProduit e);
	
	public void  delete(int id_sortie);
	
	public List<SortieProduit> findAll();
	
	public SortieProduit findById(int id_sortie);
	
}

